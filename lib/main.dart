import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:my_flutter_application/today_cases_all_response.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'COVID-19 Today',
      theme: ThemeData(primarySwatch: Colors.teal),
      home: const MyHomePage(title: 'COVID-19 Today'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<TodayCasesAllResponse>? _todayCasesAllResponse;

  @override
  void initState() {
    super.initState();
    log('init state');
    getData();
  }

  Future<void> getData() async {
    var url =
        Uri.parse('https://covid19.ddc.moph.go.th/api/Cases/today-cases-all');
    var response = await http.get(url);

    if (response.statusCode == 200) {
      setState(() {
        _todayCasesAllResponse = todayCasesAllResponseFromJson(response.body);
      });
    } else {
      throw Exception('Failed to load album');
    }
  }

  @override
  Widget build(BuildContext context) {
    log('---- Application Build ----');
    String totalCase = 'ผู้ติดเชื้อสะสมทั้งหมด';
    String totalRecovered = 'หายแล้วทั้งหมด';
    String totalDeath = 'เสียชีวิตทั้งหมด';
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: ListView(
          children: <Widget>[
            ListTile(
              title: Text(totalCase),
              subtitle:
                  Text('${_todayCasesAllResponse?[0].totalCase ?? '...'} ราย'),
            ),
            ListTile(
              title: Text(totalRecovered),
              subtitle: Text(
                  '${_todayCasesAllResponse?[0].totalRecovered ?? '...'} ราย'),
            ),
            ListTile(
              title: Text(totalDeath),
              subtitle:
                  Text('${_todayCasesAllResponse?[0].totalDeath ?? '...'} ราย'),
            ),
          ],
        ));
  }
}
