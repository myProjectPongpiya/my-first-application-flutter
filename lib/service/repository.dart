import 'dart:developer';

import 'package:http/http.dart' as http;
import 'package:my_flutter_application/today_cases_all_response.dart';

Future<void> onGetTodayCasesAllResponse() async {
  List<TodayCasesAllResponse> _todayCasesAllResponse;

  var url =
      Uri.parse('https://covid19.ddc.moph.go.th/api/Cases/today-cases-all');
  var response = await http.get(url);

  if (response.statusCode == 200) {
    _todayCasesAllResponse = todayCasesAllResponseFromJson(response.body);
    log('response: ${response.body}');
  } else {
    throw Exception('Failed to load album');
  }
}
